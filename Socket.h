#pragma once

#include <string>
#include <vector>
#include <iostream>

#include "Utils.h"
#ifdef WIN32
#include <WinSock2.h>
#define _WINSOCKAPI_
#include <Windows.h>
//#include <Ws2def.h>
#endif

using namespace std;

const int SocketBufSize = 4096;

class TInitializator
{
private:
  static int Count;
public:
  TInitializator();
  ~TInitializator();
  bool GetResult();
};

class TAddress
{
private:
  bool Result;
  void SetPort(UINT16 Port);
  UINT16 GetPort();
public:
  SOCKADDR_IN SockAddrIn;
  TAddress();
  TAddress(string Address);
  string operator =(string Address);
  const TAddress& operator=(const TAddress& Address);
  bool SetPort(TString Port);
  bool GetResult();
  TProperty<TAddress, UINT16> Port;
};

class TSocket
{
private:
  bool Result;
  bool FConnected;
  DWORD FTimeout;
  void SetConnected(bool Connected);
  bool GetConnected();
  void SetTimeout(DWORD Timeout);
  DWORD GetTimeout();
  bool CheckState(bool Connected, bool Result, bool KeepState, const char *Error = NULL);
public:
  #ifdef WIN32  
    SOCKET Socket;
  #else
    socket Socket;
  #endif
  TAddress Address;
  TSocket();
  TSocket(TAddress &Address);
  ~TSocket();
  bool Init();
  bool GetResult();
  bool Connect();
  bool Connect(TAddress &Address);
  bool Send(const char *Buf, int Length, bool KeepState = false);
  bool Send(string Buf, bool KeepState = false);
  bool Receive(vector<char> &Buf, bool KeepState = false);
  bool Receive(string &Buf, bool KeepState = false);
  string Receive(bool KeepState = true);
  string Command(const string &Command, bool KeepState = false);
  bool Command(const string &Command, string &Response, bool KeepState = false);
  TProperty<TSocket, bool> Connected;
  TProperty<TSocket, DWORD> Timeout;
};


TInitializator Initializator;
int TInitializator::Count = 0;

TInitializator::TInitializator()
{
  WSADATA wsaData;
  if(WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
  {
    cout << "WSAStartup failed.\n";
  }
  else
    Count++;
}

TInitializator::~TInitializator()
{
  WSACleanup();
  Count--;
}

bool TInitializator::GetResult()
{
  return Count > 0;
}

//TSocket
TSocket::TSocket()
{
  Connected.Init(this, &TSocket::GetConnected, &TSocket::SetConnected);
  Timeout.Init(this, &TSocket::GetTimeout, &TSocket::SetTimeout);
  Socket = NULL;
  FConnected = false;
  Result = true;
}

TSocket::TSocket(TAddress &Address)
{
  Connected.Init(this, &TSocket::GetConnected, &TSocket::SetConnected);
  Timeout.Init(this, &TSocket::GetTimeout, &TSocket::SetTimeout);
  Socket = NULL;
  FConnected = false;
  Init();
  this->Address = Address;
  Result = true;
}

TSocket::~TSocket()
{
  closesocket(Socket);
}

bool TSocket::Init()
{
  bool Result = true;
  if(Socket) return Result;
  Result = false;
  if(!Initializator.GetResult()) return Result;
  Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  if(Socket == INVALID_SOCKET)
  {
    Socket = NULL;
    cout << "Creation of the Socket Failed" << endl;
  }
  else
    Result = true;
  return Result;
}

void TSocket::SetConnected(bool Connected)
{
  if(Connected)
    FConnected = Connect();
  else
  {
    closesocket(Socket);
    Socket = NULL;
    FConnected = false;
  }
}

bool TSocket::GetConnected()
{
  return FConnected;
}

void TSocket::SetTimeout(DWORD Timeout)
{
  setsockopt(Socket, SOL_SOCKET, SO_SNDTIMEO, (char*)&Timeout, sizeof(Timeout));
  setsockopt(Socket, SOL_SOCKET, SO_RCVTIMEO, (char*)&Timeout, sizeof(Timeout));
}

DWORD TSocket::GetTimeout()
{
  //struct timeval t;
  int Size = sizeof(Timeout);
  getsockopt(Socket, SOL_SOCKET, SO_RCVTIMEO, (char*)&Timeout, &Size);
  return Timeout;
}

bool TSocket::GetResult()
{
  return Result;
}

bool TSocket::Connect(TAddress &Address)//SOCKADDR_IN &Address)
{
  this->Address = Address;
  return Connect();
}

bool TSocket::Connect()
{
  Result = true;
  if(Connected()) return Result;
  if(Socket) Connected = false;
  Init();
  if(connect(Socket, (SOCKADDR *)&(Address.SockAddrIn),
    sizeof(Address.SockAddrIn)) != 0)
  {
    cout << "Could not connect" << endl;
    Result = false;
    Connected = false;
  }
  else
    Result = true;
  FConnected = Result;
  return Result;
  return Connect(Address);
}

bool TSocket::Send(const char *Buf, int Length, bool KeepState)
{
  bool Connected = this->Connected();
  if(!Connect()) return CheckState(Connected, false, KeepState, "TSocket: Cannot connect");
  int Len = send(Socket, (const char *)&Length, sizeof(Length), 0);
  if(Len < sizeof(Length))
    return CheckState(Connected, false, KeepState, "TSocket error: cannot send data length");
  int BytesToSend = Length;
  while(BytesToSend > 0)
  {
    Len = send(Socket, Buf + Length - BytesToSend, Length, 0);
    if(Len == 0 || Len == SOCKET_ERROR)
      return CheckState(Connected, false, KeepState, "Could not send the request to the Server");
    BytesToSend -= Len;
    if(Len != Length) cout << "\rBytes sent: " << Length - BytesToSend << '/' << Length
      << "  " << flush;
  }

  return CheckState(Connected, true, KeepState);
}

bool TSocket::Send(string Buf, bool KeepState)
{
  return Send(Buf.c_str(), (int)Buf.length(), KeepState);
}

bool TSocket::CheckState(bool Connected, bool Result, bool KeepState, const char *Error /*= NULL*/)
{
  if(Error) cout << Error << endl;
  if(KeepState && !Connected || !Result) this->Connected = false;
  this->Result = Result;
  return Result;
}

bool TSocket::Receive(vector<char> &Buf, bool KeepState)
{
  bool Connected = this->Connected();
  if(!Connect()) return CheckState(Connected, false, KeepState, "TSocket: Cannot connect"); 
  int WritePtr = 0;
  int DataLen, Len;
  Len = recv(Socket, (char *)&DataLen, sizeof(DataLen), MSG_WAITALL);
  if(Len == SOCKET_ERROR || Len == 0)
    return CheckState(Connected, false, KeepState, "TSocket error: Cannot receive data size");
  if(DataLen > 10 * 1024 * 1024)
    return CheckState(Connected, false, KeepState, "TSocket error: Data size > 10MB");
  Buf.resize(DataLen);
  while(DataLen > 0)
  {
    Len = recv(Socket, &Buf[Buf.size() - DataLen], DataLen, 0);
    if(Len == SOCKET_ERROR || Len == 0)
    {
      int e = WSAGetLastError();
      if(e == WSAEWOULDBLOCK)
        return CheckState(Connected, false, KeepState, "recv failed with error: WSAEWOULDBLOCK");
      else
        return CheckState(Connected, false, KeepState,
          string(string("recv failed with error: ") + to_string((long long)e)).c_str());
    }
    DataLen -= Len;
    if(Len != Buf.size()) cout << "\rBytes received: " << Buf.size() - DataLen << '/' << Buf.size()
      << "  " << flush;
  }
  return CheckState(Connected, true, KeepState);
}

string TSocket::Receive(bool KeepState)
{
  vector<char> b;
  Receive(b);
  return &b[0];
}

bool TSocket::Receive(string &Buf, bool KeepState)
{
  vector<char> b;
  if(Receive(b, KeepState))
    Buf = string(b.begin(), b.end());
  else
    return false;
  return true;
}

bool TSocket::Command(const string &Command, string &Response, bool KeepState)
{
  bool State = Connected();
  bool Result = false;
  if(Send(Command, false))
    if(Receive(Response, false))
      Result = true;
  if(KeepState && !State) this->Connected = false;
  return Result;
}

std::string TSocket::Command(const string &Command, bool KeepState)
{
  string Response;
  return this->Command(Command, Response, KeepState) ? Response : "";
}

//TAddress
TAddress::TAddress()
{
  Port.Init(this, &TAddress::GetPort, &TAddress::SetPort);
  Result = Initializator.GetResult();
}

TAddress::TAddress(string Address)
{
  Port.Init(this, &TAddress::GetPort, &TAddress::SetPort);
  Result = Initializator.GetResult();
  (*this) = Address;
}

std::string TAddress::operator=(string Address)
{
  size_t p = Address.rfind(':');
  if(p != string::npos)
  {
    TString s(Address.substr(p + 1));
    int i = s.ToInt();
    if(p >= 0 || p <= 65535)
      Port = (UINT16)i;
  }
  struct addrinfo hints;
  ZeroMemory(&hints, sizeof(hints));
  hints.ai_family = AF_INET;          // We are targeting IPv4
  hints.ai_protocol = IPPROTO_TCP;    // We are targeting TCP
  hints.ai_socktype = SOCK_STREAM;    // We are targeting TCP so its SOCK_STREAM
  struct addrinfo* AddressInfo = NULL;
  if(getaddrinfo(Address.c_str(), NULL, &hints, &AddressInfo) || AddressInfo == NULL)
  {
    cout << "Could not resolve the Host Name" << endl;
    Result = false;
  }
  IN_ADDR a;
  a = ((struct sockaddr_in*) AddressInfo->ai_addr)->sin_addr;
  freeaddrinfo(AddressInfo);

  SockAddrIn.sin_addr = a;
  SockAddrIn.sin_family = AF_INET;

  return Address;
}


const TAddress& TAddress::operator=(const TAddress& Address)
{
  SockAddrIn = Address.SockAddrIn;
  Result = Address.Result;
  return Address;
}


bool TAddress::SetPort(TString Port)
{
  int p = Port.ToInt();
  if(p != -1)
  {
    if(p < 0 || p > 65535)
      return false;
    this->Port = (UINT16)p;
    SockAddrIn.sin_port = htons((UINT16)p);
    return true;
  }
  else
    return false;
}

void TAddress::SetPort(UINT16 Port)
{
  SockAddrIn.sin_port = htons(Port);
}

UINT16 TAddress::GetPort()
{
  return ntohs(SockAddrIn.sin_port);
}

bool TAddress::GetResult()
{
  return Result;
}

//SOCKADDR_IN TAddress::ToSockAddrIn()
//{
//  SOCKADDR_IN SockAddr;
//  SockAddr.sin_addr = Address;
//  SockAddr.sin_family = AF_INET;
//  SockAddr.sin_port = htons(Port);
//  return SockAddr;
//}

//IN_ADDR TAddress::ToInAddr()
//{
//  return Address;
//}

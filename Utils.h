#pragma once

#include <string>
#include <list>
#include <vector>
#include <stdexcept>
#include <stdint.h>

#include "Properties.h"

//namespace Utils {


using namespace std;

class TString : public string
{
public:
  TString() {}
  TString(string String)
  {
    this->assign(String);
  }
  TString(const char *String)
  {
    this->assign(String);
  }
  TString(int Value)
  {
    this->assign(std::to_string((long double)Value));
  }
  TString operator =(string String)
  {
    this->assign(String);
  }
  bool ToInt(int &Value)
  {
    try
    {
      Value = stoi(*this);
    }
    catch(...)
    {
      return false;
    }
    return true;
  }

  int ToInt()
  {
    int Result;
    return (ToInt(Result) ? Result : -1);
  }

  bool ToLong(long &Value)
  {
    try {
      Value = stol(*this);
    }
    catch(...)
    {
      return false;
    }
    return true;
  }

  long ToLong()
  {
    long Result;
    return ToLong(Result) ? Result : -1;
  }
};

class TStringList
{
private:
  char Delimeter;
  void Init()
  {
    Delimeter = ',';
    Text.Init(this, &TStringList::GetText, &TStringList::SetText);
    CommaText.Init(this, &TStringList::GetCommaText, &TStringList::SetCommaText);
    Values.Init(this, &TStringList::GetValue, &TStringList::SetValue);
  }

  void SetText(string Text, char Delimeter,  bool AllowEmptyValue = true)
  {
    if(!Delimeter) Delimeter = '\n';
    size_t Pos = 0;
    size_t NewPos = Text.find(Delimeter, Pos);
    while (NewPos != string::npos)
    {
      if(AllowEmptyValue || NewPos != Pos)
        Items.push_back(Text.substr(Pos, NewPos - Pos));
      Pos = NewPos + 1;
      NewPos = Text.find(Delimeter, Pos);
    }
    if(AllowEmptyValue || NewPos != Pos)
      Items.push_back(Text.substr(Pos));
  }
  string GetText(char Delimeter)
  {
    string Result;
    for(auto i = Items.begin(); i != Items.end(); i++)
      Result += *i + (Delimeter ? Delimeter : '\n');
    return Result;
  }

  void SetText(string Text)
  {
    SetText(Text, 0);
  }
  string GetText()
  {
    return GetText(0);
  }

  void SetCommaText(string Text)
  {
    SetText(Text, Delimeter);
  }
  string GetCommaText()
  {
    return GetText(Delimeter);
  }

  void SetValue(string Key, TString Value)
  {
    if(Key.empty()) return;
    for(auto i = Items.begin(); i != Items.end(); i++)
      if(i->substr(0, Key.length()) == Key)
        if(i->length() == Key.length() || (*i)[Key.length()] == '=')
        {
          *i += '=' + Value;
          return;
        }
    Items.push_back(Key + '=' + Value);
  }
  TString GetValue(string Key)
  {
    if(Key.empty()) return "";
    for(auto i = Items.begin(); i != Items.end(); i++)
      if(i->substr(0, Key.length()) == Key)
        if((*i)[Key.length()] == '=' && i->length() >= Key.length() + 2)
          return i->substr(Key.length() + 1, i->length() - Key.length() - 1);
    return "";
  }
public:
  typedef list<string> TItems;
  TItems Items;

  TStringList()
  {
    Init();
  }

  TStringList(string &Text, char Delimeter = 0, bool AllowEmptyValue = true)
  {
    Init();
    if(Delimeter)
      this->Delimeter = Delimeter;
    SetText(Text, Delimeter, AllowEmptyValue);
  }

  void Add(string String)
  {
    Items.push_back(String);
  }

  void Clear()
  {
    Items.clear();
  }

  bool ItemExists(string Key)
  {
    if(Key.empty()) return false;
    for(auto i = Items.begin(); i != Items.end(); i++)
      if(i->substr(0, Key.length()) == Key)
        if((*i)[Key.length()] == '=' || i->length() == Key.length())
          return true;
  }
  void SetDelimeter(char Delimeter)
  {
    this->Delimeter = Delimeter;
  }
  TItems::iterator GetFirst()
  {
    return Items.begin();
  }
  bool GetNext(TItems::iterator &Iterator)
  {
    Iterator++;
    return Iterator != Items.end();
  }

  TProperty <TStringList, string> Text;
  TProperty <TStringList, string> CommaText;
  TIndexedProperty<TStringList, string, TString> Values;
};

static uint32_t GetHash(std::vector<char> &Vector, int Step = 1)
{
  uint32_t Result = 0;
  for(size_t i = 0; i < Vector.size(); i += Step) 
    Result = Result * 31 + Vector[i];
  return Result;
}

//bool Hash(string FileName, uint32_t &OutHash, int Step = 1)
//{
//  ifstream BruteBot(BotServer.BruteBotFile, ios::binary | ios::ate);
//  if(!BruteBot)
//  {
//    cout << "Error: cannot open " << BotServer.BruteBotFile << endl;
//    return false;
//  }
//  uint32_t Result = 0;
//  for(int i = 0; i < Vector.size(); i += Step)
//    Result = Result * 31 + Vector[i];
//  return Result;
//}
//}

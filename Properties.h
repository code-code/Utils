#pragma once
#include <assert.h>

template <typename TOwner, typename TValue>
class TProperty
{
private:
  typedef TValue (TOwner::*TGet)();
  typedef void (TOwner::*TSet)(TValue);
  TOwner* Owner;
  TGet Get;
  TSet Set;
  TValue *ValuePtr;
public:
  TProperty()
  {
    Owner = NULL;
    Set = NULL;
    Get = NULL;
    ValuePtr = NULL;
  }
  void Init(TOwner* Owner, TGet Get, TSet Set)
  {
    this->Owner = Owner;
    this->Set = Set;
    this->Get = Get;
  }
  void Init(TOwner* Owner, TValue &ValueRef, TSet Set)
  {
    this->Owner = Owner;
    this->Set = Set;
    this->ValuePtr = &ValueRef;
  }
  TValue operator =(const TValue& Value)
  {
    assert(Owner != NULL);
    assert(Set != NULL);
    (Owner->*Set)(Value);
    return Value;
  }
  TValue operator ()()
  {
    assert(Owner != NULL);
    if(Get != NULL)
      return (Owner->*Get)();
    else if (ValuePtr != NULL)
      return *ValuePtr;
    else
      assert(false);
    return *ValuePtr;
  }
  operator TValue()
  {
    assert(Owner != NULL);
    if(Get != NULL)
      return (Owner->*Get)();
    else if (ValuePtr != NULL)
      return *ValuePtr;
    else
      assert(false);
    return *ValuePtr;
  }
};

template <typename TOwner, typename TKey, typename TValue>
class TIndexedProperty
{
private:
  typedef TValue (TOwner::*TIndexedGet)(TKey);
  typedef void (TOwner::*TIndexedSet)(TKey, TValue);
  TOwner* Owner;
  TIndexedGet Get;
  TIndexedSet Set;
public:
  TIndexedProperty()
  {
    Owner = NULL;
    Set = NULL;
    Get = NULL;
    //ValuePtr = NULL;
  }

  void SetStub(TKey Key, TValue Value)
  {
    (Owner->*Set)(Key, Value);
  }

  TValue GetStub(TKey Key)
  {
    return (Owner->*Get)(Key);
  }

  class TProxy
  {
  public:
    TIndexedProperty &IndexedProperty;
    TKey Key;

    TProxy(TIndexedProperty &IndexedProperty, TKey &Key)
      : IndexedProperty(IndexedProperty), Key(Key) {}

    TValue operator =(const TValue& Value)
    {
      assert(IndexedProperty.Owner != NULL);
      assert(IndexedProperty.Set != NULL);
      IndexedProperty.SetStub(Key, Value);
      return Value;
    }

    operator TValue()
    {
      assert(IndexedProperty.Owner != NULL);
      if(IndexedProperty.Get != NULL)
        return IndexedProperty.GetStub(Key);
      //       else if (ValuePtr != NULL)
      //         return *ValuePtr;
      else
        assert(false);
    }

    TValue operator()()
    {
      assert(IndexedProperty.Owner != NULL);
      if(IndexedProperty.Get == NULL) assert(false);
      //       else if (ValuePtr != NULL)
      //         return *ValuePtr;
      return IndexedProperty.GetStub(Key);
    }
  };

  void Init(TOwner* Owner, TIndexedGet Get, TIndexedSet Set)
  {
    this->Owner = Owner;
    this->Set = Set;
    this->Get = Get;
  }

  TValue operator =(const TValue& Value)
  {
    assert(Owner != NULL);
    assert(Set != NULL);
    (Owner->*Set)(Value, 2);
    return Value;
  }

  TProxy operator[](TKey Key)
  {
    return TProxy(*this, Key);
  }
};
